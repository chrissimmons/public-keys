# Public Keys

**Do not trust commits which are not GPG signed.**

This repository contains Chris Simmons' public keys.

## SSH

- See `ssh/authorized_keys` for a formatted file for pulling into a user's `.ssh/authorized_keys` file
- As this repository is set to only permit signed commits, it is reasonably safe to run a simple script locally to get this file

## Quick steps

- Go to your `.ssh` folder
- Remove any existing `authorized_keys` file
- Run `wget https://chrissimmons.keybase.pub/ssh/authorized_keys`

You should now have the updated `authorized_keys` file.

To test this:

- DO NOT YET LOG OUT OF YOUR CURRENT SESSION
- Open a new session and try to `ssh youruser@machine`
- Ensure login without issues

## Adding a new public key

### KeyBase File System

- Copy to `%KBFS%/public/chrissimmons/ssh/authorized_keys`
  - `/keybase/public/chrissimmons/ssh/authorized_keys` on Mac / Linux
  - `K:\public\chrissimmons\ssh\authorized_keys` on Windows
- `wget https://chrissimmons.keybase.pub/ssh/authorized_keys`

### Git

The public key should represent the identity of Chris Simmons on a given machine.

- Clone this repo to the machine where you're creating the key
- Generate the new key (It is assumed you have a `ssh` installation set up on the machine)
  - `ssh-keygen -C USER@HOST.20181230 -f 20181230`
    - `USER` - Your username, no spaces (i.e. "Chris")
    - `HOST` - The machine name (e.g. "OFFICE" or "raspberrypi01")
    - `YYYYMMDD` - The encoded date
    - Do not use a passphrase
    - This will create two files in the current directory (with date-encoding):
      - `YYYYMMDD`
      - `YYYYMMDD.pub`
- Append the contents of `YYYYMMDD.pub` to `ssh\authorized_keys` in this repo
  - The contents simply go on a new line at the end
- Commit the change `ssh\authorized_keys`, ensuring the commit is signed
  - If the current machine is not set up for that, there are instructions in the next section

### Signing commits

- Install gpg on the machine
  - Windows:
    - https://gnupg.org/download/
    - https://gpg4win.org/download.html
- `gpg --list-secret-keys --keyid-format LONG` (this may also be `gpg2`, depending on your install)
- You'll see output with a line like this in it:
  - `sec   rsa4096/4BDED23953BA34B6 2018-12-30 [SC]`
- `4BDED23953BA34B6` is the key identifier, so run this to instruct git which key to attach to signed commits:
  - `git config --global user.signingkey 4BDED23953BA34B6`
- Tell git what gpg binary to use:
  `git config --global gpg.program path/to/gpg`
- Tell git to sign commits, but _only in this repo_:
  `git config commit.gpgsign true`
  - It is _highly_ unlikely that you want to set this bit globally
- Record the public key:
  - `gpg --armor --export 4BDED23953BA34B6` (use the key ID you got above)
  - Copy into this repo at `gpg\YYYYMMDD.HOST.txt`
  - Submit to GitLab and GitHub
  - You can optionally submit to public GPG key repositories

### Get the new keyset on your target machines

#### Method 1 - pull down authorized_keys from well-known URL, replacing set

_This assumes that you want to completely replace the `authorized_keys` file on the machine._

- SSH to the target machine.
  - If the machine requires an SSH key for login (and trusts your _old_ key), indicate the old key (assuming you backed up) as follows:
    - `ssh -i ~/.ssh/backup.YYYYMMDD/id_rsa user@host`
- `wget https://github.com/ChrisSimmons/public-keys/blob/master/ssh/authorized_keys -O ~/.ssh/authorized_keys`
- `chmode 600 ~/.ssh/authorized_keys`

#### Method 2 - ssh-copy-id

This method is easier if:

- The target machine supports username/password login
- You want to append your local key only, not replace `authorized_keys`

From the machine you just created the key:

`ssh-copy-id -i ~/.ssh/id_rsa user@host`

##### Reference

- See https://www.ssh.com/ssh/copy-id

### Replace your local default key

If you had a key on the machine initially, ensure that you log into each host where you would normally log in from this source machine and get the new key downloaded.  Once that's done, you're safe to use the new key as your default.  (All syntax is in bash. It is assumed that you can translate to Windows as needed).

- Copy the keypair (unix syntax used here):
  - `cp YYYYMMDD ~/.ssh/id_rsa`
  - `cp YYYYMMDD.pub ~/.ssh/id_rsa.pub`
- Set permissions as directed below

### File permissions

| File              | Permissions                        |                                    |
| ---               | ---                                | ---                                |
| `~/.ssh`          | `chmod 700 ~/.ssh`                 | Writable only by owner             |
| `authorized_keys` | `chmod 600 ~/.ssh/authorized_keys` | Readable only by owner             |
| `id_rsa`          | `chmod 600 ~/.ssh/id_rsa`          | Readable only by owner             |
| `id_rsa.pub`      | `chmod 644 ~/.ssh/id_rsa.pub`      | Writable by owner, readable by all |

Folder should look like this:

```
drwx------ 2 chris chris 4096 Dec 30 16:36 .
drwxr-xr-x 4 chris chris 4096 Dec 30 14:43 ..
-rw------- 1 chris chris 2346 Dec 29 23:08 authorized_keys
-rw------- 1 chris chris 1679 Dec 29 23:08 id_rsa
-rw-r--r-- 1 chris chris  408 Dec 29 23:08 id_rsa.pub
```

### Prune `ssh\authorized_keys`

Once you are certain that the previous public key from your machine is no longer being used:

- Prune it from `ssh\authorized_keys`
- Replace the file on each target machine:
  - `wget https://github.com/ChrisSimmons/public-keys/blob/master/ssh/authorized_keys -O ~/.ssh/authorized_keys`

## Log

- Initial entry
